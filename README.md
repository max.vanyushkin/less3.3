

Описание задания
1. Модифицировать плейбук из прошлого урока в роль
2. Сделать темплейт конфига нжингс (джинжа2)
Переменные - server_name == test1.juneway.pro test2.juneway.pro
Должен быть один шаблон на 2 виртуальных хоста.
( Сервер1 = test1..... Сервер2 = test2...)
Конфиг копировать по стандартному пути нжингса.
Сам конфиг
server {
listen 80;
listen [::]:80;
server_name ваша_переменная;
root /var/www/ваша_переменная;
index index.html;
try_files $uri /index.html;
}
Переменные должы храниться в варс файле роле
Итог - 2 конфига на каждом из серверов.

Ответ

====================

Развернуто 2 машины
  - 34.65.36.97
  - 34.65.197.131

Как протестировать 

 
	curl --header 'Host: test1.juneway.pro' http://34.65.36.97;
	curl --header 'Host: test1.juneway.pro' http://34.65.197.131;
	curl --header 'Host: test2.juneway.pro' http://34.65.36.97;
	curl --header 'Host: test2.juneway.pro' http://34.65.197.131;
 
Структура связанных директорий на первой машине

	root@less-3-3-1:~# ls -l /var/www
	total 12
	drwxr-xr-x 2 root root 4096 Jan 11 08:52 html
	drwxr-xr-x 2 root root 4096 Jan 11 08:53 test1.juneway.pro
	drwxr-xr-x 2 root root 4096 Jan 11 08:53 test2.juneway.pro

	root@less-3-3-1:~# ls -l /etc/nginx/sites-enabled/
	total 0
	lrwxrwxrwx 1 root root 34 Jan 11 08:52 default -> /etc/nginx/sites-available/default
	lrwxrwxrwx 1 root root 55 Jan 11 08:53 nginx_test1.juneway.pro.conf -> /etc/nginx/sites-available/nginx_test1.juneway.pro.conf
	lrwxrwxrwx 1 root root 55 Jan 11 08:53 nginx_test2.juneway.pro.conf -> /etc/nginx/sites-available/nginx_test2.juneway.pro.conf

	root@less-3-3-1:~# ls -l /etc/nginx/sites-available/
	total 12
	-rw-r--r-- 1 root root 2412 Aug 25  2020 default
	--w----r-T 1 root root  171 Jan 11 08:52 nginx_test1.juneway.pro.conf
	--w----r-T 1 root root  171 Jan 11 08:53 nginx_test2.juneway.pro.conf


Структура связанных директорий на второй машине

	root@less-3-3-2:~# ls -l /var/www
	total 12
	drwxr-xr-x 2 root root 4096 Jan 11 08:52 html
	drwxr-xr-x 2 root root 4096 Jan 11 08:53 test1.juneway.pro
	drwxr-xr-x 2 root root 4096 Jan 11 08:53 test2.juneway.pro

	root@less-3-3-2:~# ls -l /etc/nginx/sites-enabled/
	total 0
	lrwxrwxrwx 1 root root 34 Jan 11 08:52 default -> /etc/nginx/sites-available/default
	lrwxrwxrwx 1 root root 55 Jan 11 08:53 nginx_test1.juneway.pro.conf -> /etc/nginx/sites-available/nginx_test1.juneway.pro.conf
	lrwxrwxrwx 1 root root 55 Jan 11 08:53 nginx_test2.juneway.pro.conf -> /etc/nginx/sites-available/nginx_test2.juneway.pro.conf

	root@less-3-3-2:~# ls -l /etc/nginx/sites-available/
	total 12
	-rw-r--r-- 1 root root 2412 Aug 25  2020 default
	--w----r-T 1 root root  171 Jan 11 08:52 nginx_test1.juneway.pro.conf
	--w----r-T 1 root root  171 Jan 11 08:53 nginx_test2.juneway.pro.conf
